package com.vesodev.dndtoolsui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DndToolsUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DndToolsUiApplication.class, args);
	}

}
