package com.vesodev.dndtoolsdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DndToolsDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DndToolsDiscoveryApplication.class, args);
	}

}
