package com.vesodev.dndtoolsauth.jpa;

import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class AuthMethodSetToStringConverter implements AttributeConverter<Set<ClientAuthenticationMethod>, String> {

    @Override
    public String convertToDatabaseColumn(Set<ClientAuthenticationMethod> attribute) {
        return attribute == null ? null : attribute.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    @Override
    public Set<ClientAuthenticationMethod> convertToEntityAttribute(String dbData) {
        return dbData == null ? Collections.emptySet() : Arrays.stream(dbData.split(",")).map(ClientAuthenticationMethod::new).collect(Collectors.toSet());
    }
}
