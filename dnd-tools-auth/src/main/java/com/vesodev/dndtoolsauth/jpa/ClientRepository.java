package com.vesodev.dndtoolsauth.jpa;

import com.vesodev.dndtoolsauth.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, String> {

    public Optional<Client> findByClientId(String clientId);
}
