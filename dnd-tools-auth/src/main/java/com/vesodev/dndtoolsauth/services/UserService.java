package com.vesodev.dndtoolsauth.services;

import com.vesodev.dndtoolsauth.jpa.UserRepository;
import com.vesodev.dndtoolsauth.model.User;
import com.vesodev.dndtoolsauth.model.UserDetailsDto;
import com.vesodev.dndtoolsauth.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetailsDto loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByEmail(username).orElseThrow(EntityNotFoundException::new);

        return UserDetailsDto.builder()
                .active(user.getAccountActive())
                .authorities(user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toSet()))
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }

    public User createUser(UserDto userDto) {
        User user = User.builder()
                .roles(Set.of("USER"))
                .email(userDto.getEmail())
                .name(userDto.getName())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .accountActive(true).build();
        return this.userRepository.save(user);
    }

    public Optional<User> findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public OidcUserInfo getOidUserInfo(String username) {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new EntityNotFoundException("User not found"));
        return OidcUserInfo.builder().email(user.getEmail()).name(user.getName()).preferredUsername(user.getEmail()).claims(claims -> claims.put("uuid", user.getId())).build();
    }
}
