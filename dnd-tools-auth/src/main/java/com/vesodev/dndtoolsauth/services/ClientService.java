package com.vesodev.dndtoolsauth.services;

import com.vesodev.dndtoolsauth.jpa.ClientRepository;
import com.vesodev.dndtoolsauth.model.Client;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

public class ClientService implements RegisteredClientRepository {

    private final ClientRepository clientRepository;

    private final PasswordEncoder passwordEncoder;

    public ClientService(ClientRepository clientRepository, PasswordEncoder passwordEncoder) {
        this.clientRepository = clientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void save(RegisteredClient registeredClient) {

    }

    @Override
    public RegisteredClient findById(String id) {
        return this.buildClient(this.clientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Client not found")));
    }

    @Override
    public RegisteredClient findByClientId(String clientId) {
        return this.buildClient(this.clientRepository.findByClientId(clientId).orElseThrow(() -> new EntityNotFoundException("Client not found")));
    }

    private RegisteredClient buildClient(Client client) {
        return RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("api-client")
                .clientSecret(client.getClientSecret())
                .clientName(client.getClientName())
                .clientAuthenticationMethods(clientAuthenticationMethods -> clientAuthenticationMethods = client.getClientAuthenticationMethods())
                .authorizationGrantTypes(authorizationGrantTypes -> authorizationGrantTypes = client.getAuthorizationGrantTypes())
                .redirectUris(redirectUris -> redirectUris = client.getRedirectUris())
                .scopes(strings -> strings = client.getScopes())
                .build();
    }
}
