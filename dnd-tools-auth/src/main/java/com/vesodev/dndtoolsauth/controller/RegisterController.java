package com.vesodev.dndtoolsauth.controller;

import com.vesodev.dndtoolsauth.model.UserDto;
import com.vesodev.dndtoolsauth.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegisterController {

    private final UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("register")
    public String registrationForm(Model model) {
        model.addAttribute("user", UserDto.builder().build());
        return "register";
    }

    @PostMapping("/register")
    public String registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto) {
        userService.createUser(userDto);
        return "login";
    }
}
