package com.vesodev.dndtoolsauth.model;

import com.vesodev.dndtoolsauth.jpa.SetToStringConverter;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Convert;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@Builder
public class UserDto {

    @NotEmpty
    @NotEmpty
    private String name;

    @NotEmpty
    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @NotEmpty
    private String password;

    private Set<String> roles;

    private Boolean accountActive;

}
