package com.vesodev.dndtoolsauth.model;

import com.vesodev.dndtoolsauth.jpa.AuthGrantTypeSetToString;
import com.vesodev.dndtoolsauth.jpa.AuthMethodSetToStringConverter;
import com.vesodev.dndtoolsauth.jpa.SetToStringConverter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;
import org.springframework.security.oauth2.server.authorization.config.TokenSettings;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Entity
public class Client {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "clientId", columnDefinition = "nvarchar(512)")
    private String clientId;

    private Instant clientIdIssuedAt;

    @Column(name = "clientSecret", columnDefinition = "nvarchar(512)")
    private String clientSecret;

    private Instant clientSecretExpiresAt;

    private String clientName;

    @Convert(converter = AuthMethodSetToStringConverter.class)
    private Set<ClientAuthenticationMethod> clientAuthenticationMethods;

    @Convert(converter = AuthGrantTypeSetToString.class)
    private Set<AuthorizationGrantType> authorizationGrantTypes;

    @Convert(converter = SetToStringConverter.class)
    private Set<String> redirectUris;

    @Convert(converter = SetToStringConverter.class)
    private Set<String> scopes;


    public String getId() {
        return id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Instant getClientIdIssuedAt() {
        return clientIdIssuedAt;
    }

    public void setClientIdIssuedAt(Instant clientIdIssuedAt) {
        this.clientIdIssuedAt = clientIdIssuedAt;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public Instant getClientSecretExpiresAt() {
        return clientSecretExpiresAt;
    }

    public void setClientSecretExpiresAt(Instant clientSecretExpiresAt) {
        this.clientSecretExpiresAt = clientSecretExpiresAt;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Set<ClientAuthenticationMethod> getClientAuthenticationMethods() {
        return clientAuthenticationMethods;
    }

    public void setClientAuthenticationMethods(Set<ClientAuthenticationMethod> clientAuthenticationMethods) {
        this.clientAuthenticationMethods = clientAuthenticationMethods;
    }

    public Set<AuthorizationGrantType> getAuthorizationGrantTypes() {
        return authorizationGrantTypes;
    }

    public void setAuthorizationGrantTypes(Set<AuthorizationGrantType> authorizationGrantTypes) {
        this.authorizationGrantTypes = authorizationGrantTypes;
    }

    public Set<String> getRedirectUris() {
        return redirectUris;
    }

    public void setRedirectUris(Set<String> redirectUris) {
        this.redirectUris = redirectUris;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public void setScopes(Set<String> scopes) {
        this.scopes = scopes;
    }

    public static Client from(RegisteredClient registeredClient) {
        Client client = new Client();
        client.setClientId(registeredClient.getClientId());
        client.setClientName(registeredClient.getClientName());
        client.setClientSecret(registeredClient.getClientSecret());
        client.setClientAuthenticationMethods(registeredClient.getClientAuthenticationMethods());
        client.setAuthorizationGrantTypes(registeredClient.getAuthorizationGrantTypes());
        client.setRedirectUris(registeredClient.getRedirectUris());
        client.setScopes(registeredClient.getScopes());
        return client;
    }
}
