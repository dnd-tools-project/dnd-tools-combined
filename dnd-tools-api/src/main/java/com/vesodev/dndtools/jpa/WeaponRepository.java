package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 24.6.2021 г..
 */
public interface WeaponRepository extends JpaRepository<Weapon, String> {
}
