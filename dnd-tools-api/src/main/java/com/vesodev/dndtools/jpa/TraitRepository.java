package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Trait;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by vesko on 30.6.2021 г..
 */
public interface TraitRepository extends JpaRepository<Trait, String> {
    public List<Trait> findAllByNameContains(String name);

}
