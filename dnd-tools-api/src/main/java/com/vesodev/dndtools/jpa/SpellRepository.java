package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Spell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Repository
public interface SpellRepository extends JpaRepository<Spell, String> {

    @Query(value = "select * from Spells where classes like CONCAT('%',:classes,'%') and spell_slot <= :spellSlot order by spell_slot, name", nativeQuery = true)
    public List<Spell> findByClassesContainsAndSpellSlotLessThanEqual(@Param("classes") String classes, @Param("spellSlot") Integer spellSlot);

    @Query(value = "select * from Spells order by spell_slot, name", nativeQuery = true)
    List<Spell> getAll();

    @Query(value = "select * from Spells where name like concat('%',:name,'%') order by spell_slot, name", nativeQuery = true)
    List<Spell> findAllByNameContains(@Param("name") String name);
}
