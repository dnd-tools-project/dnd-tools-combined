package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Character;
import com.vesodev.dndtools.model.CharacterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, String> {

    public Character findByName(String name);

    public List<Character> findAllByNameLike(String name);

    public List<CharacterView> findAllByPlayerId(String id);

}
