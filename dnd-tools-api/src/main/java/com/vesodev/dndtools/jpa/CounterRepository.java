package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 17.6.2021 г..
 */
public interface CounterRepository extends JpaRepository<Counter, String> {
}
