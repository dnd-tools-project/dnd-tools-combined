package com.vesodev.dndtools.model;

import javax.persistence.*;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Entity
@Table(name = "spells")
public class Spell extends DndObject {

    @Column(length = 100000)
    private String description;

    private Integer spellSlot;

    private String duration;

    @Column(length = 10000)
    private String components;

    private String castTime;

    @Column(name = "spell_range")
    private String range;

    @Column(name = "spell_classes")
    private String classes;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSpellSlot() {
        return spellSlot;
    }

    public void setSpellSlot(Integer spellSlot) {
        this.spellSlot = spellSlot;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getCastTime() {
        return castTime;
    }

    public void setCastTime(String castTime) {
        this.castTime = castTime;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }
}
