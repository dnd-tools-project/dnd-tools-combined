package com.vesodev.dndtools.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vesko on 25.6.2021 г..
 */
@Entity
public class Trait extends DndObject {


    @Column(length = 3000)
    private String description;

    @ManyToMany
    private List<Proficiency> proficiencies;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Proficiency> getProficiencies() {
        return proficiencies;
    }

    public void setProficiencies(List<Proficiency> proficiencies) {
        this.proficiencies = proficiencies;
    }
}
