package com.vesodev.dndtools.model;


import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by vesko on 25.5.2021 г..
 */
@Entity
public class Weapon extends Item{

    @OneToOne
    private Dice damage;

    private String damageType;

    public Weapon() {
        setCount(1);
    }

    public Dice getDamage() {
        return damage;
    }

    public void setDamage(Dice damage) {
        this.damage = damage;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }
}
