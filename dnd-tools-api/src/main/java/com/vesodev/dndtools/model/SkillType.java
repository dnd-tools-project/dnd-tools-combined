package com.vesodev.dndtools.model;

/**
 * Created by vesko on 22.5.2021 г..
 */
public enum SkillType {
    Acrobatics, AnimalHandling, Arcana,
    Athletics, Deception, History, Insight,
    Intimidation, Investigation, Medicine,
    Nature, Perception, Performance, Persuasion,
    Religion, SleightOfHand, Stealth, Survival
}
