package com.vesodev.dndtools.model;

import javax.persistence.*;

/**
 * Created by vesko on 30.6.2021 г..
 */
@Entity
public class Proficiency extends DndObject {

    @Column(length = 3000)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
