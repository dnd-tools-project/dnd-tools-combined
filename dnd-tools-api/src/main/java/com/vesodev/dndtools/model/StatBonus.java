package com.vesodev.dndtools.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by vesko on 25.6.2021 г..
 */
//@Entity
public class StatBonus extends DndObject {

    private StatType statType;

    private Integer bonus;


    public StatType getStatType() {
        return statType;
    }

    public void setStatType(StatType statType) {
        this.statType = statType;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }
}
