package com.vesodev.dndtools.model;

import javax.persistence.*;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Entity
@Table(name = "skills")
public class Skill extends DndObject{

    private StatType statType;

    private SkillType skillType;

    private boolean hasProficiency = false;



    public StatType getStatType() {
        return statType;
    }

    public SkillType getSkillType() {
        return skillType;
    }

    public void setSkillType(SkillType skillType) {
        this.skillType = skillType;
        if(SkillType.Acrobatics.equals(skillType) || SkillType.SleightOfHand.equals(skillType) || SkillType.Stealth.equals(skillType)){
            this.statType = StatType.Dexterity;
        }else if(SkillType.AnimalHandling.equals(skillType) || SkillType.Insight.equals(skillType) || SkillType.Medicine.equals(skillType) ||
                SkillType.Perception.equals(skillType) || SkillType.Survival.equals(skillType)){
            this.statType = StatType.Wisdom;
        }else if(SkillType.Arcana.equals(skillType) || SkillType.History.equals(skillType) || SkillType.Investigation.equals(skillType) ||
                SkillType.Nature.equals(skillType) || SkillType.Religion.equals(skillType)){
            this.statType = StatType.Intelligence;
        }else if(SkillType.Athletics.equals(skillType)){
            this.statType = StatType.Strength;
        }else if (SkillType.Deception.equals(skillType) || SkillType.Intimidation.equals(skillType) || SkillType.Performance.equals(skillType) ||
                SkillType.Persuasion.equals(skillType)){
            this.statType = StatType.Charisma;
        }
    }

    public Boolean getHasProficiency() {
        return hasProficiency;
    }

    public void setHasProficiency(Boolean hasProficiency) {
        this.hasProficiency = hasProficiency;
    }
}
