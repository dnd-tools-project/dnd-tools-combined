package com.vesodev.dndtools.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vesko on 7.7.2021 г..
 */
@Entity
public class DnDCampaign extends DndObject{

    @ManyToMany
    private List<Character> characters;

    @ManyToMany
    private List<Npc> npcs;

    @ManyToMany
    private List<Item> items;


    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    public List<Npc> getNpcs() {
        return npcs;
    }

    public void setNpcs(List<Npc> npcs) {
        this.npcs = npcs;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
