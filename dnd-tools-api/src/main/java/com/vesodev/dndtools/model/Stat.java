package com.vesodev.dndtools.model;

import javax.persistence.*;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Entity
@Table(name = "stats")
public class Stat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private StatType type;

    private Integer value;

    private Integer bonus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StatType getType() {
        return type;
    }

    public void setType(StatType type) {
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
        float tmp = (value - 10.0f) / 2.0f;
        boolean isNegative = tmp < 0;
        int bonus = (int) Math.floor(Math.abs(tmp));
        this.bonus = isNegative ? bonus * -1 : bonus;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        float tmp = (value - 10.0f) / 2.0f;
        boolean isNegative = tmp < 0;
        bonus = (int) Math.floor(Math.abs(tmp));
        this.bonus = isNegative ? bonus * -1 : bonus;
    }
}
