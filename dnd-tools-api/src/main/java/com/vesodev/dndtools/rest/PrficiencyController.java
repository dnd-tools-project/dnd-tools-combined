package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.ProficiencyRepository;
import com.vesodev.dndtools.model.Proficiency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vesko on 30.6.2021 г..
 */
@RestController
//@RequestMapping(path = "/api")
public class PrficiencyController {

    @Autowired
    private ProficiencyRepository proficiencyRepository;

    @PostMapping(path = "/proficiencies")
    public Proficiency addProf(@RequestBody Proficiency proficiency) {
        return this.proficiencyRepository.save(proficiency);
    }

    @GetMapping(path = "/proficiencies")
    public List<Proficiency> getProf() {
        return (List<Proficiency>) this.proficiencyRepository.findAll();
    }

    @GetMapping(path = "/proficiencies/{profName}")
    public List<Proficiency> findByName(@PathVariable("profName") String profName) {
        return this.proficiencyRepository.findAllByNameContains(profName);
    }

}
