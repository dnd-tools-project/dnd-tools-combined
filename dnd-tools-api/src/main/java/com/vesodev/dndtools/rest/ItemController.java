package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.DiceRepository;
import com.vesodev.dndtools.jpa.ItemRepository;
import com.vesodev.dndtools.model.Item;
import com.vesodev.dndtools.model.Weapon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 2.6.2021 г..
 */
@RestController
//@RequestMapping(path = "/api")
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private DiceRepository diceRepository;

    @PutMapping(path = "/items")
    public Item saveItem(@RequestBody Item item) {
        if (item.getClass().equals(Weapon.class)) {
            diceRepository.save(((Weapon) item).getDamage());
        }
        return this.itemRepository.save(item);
    }
}
