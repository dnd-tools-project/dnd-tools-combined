package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.DnDCampaignRepository;
import com.vesodev.dndtools.model.DnDCampaign;
import com.vesodev.dndtools.model.DnDCampaignView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by vesko on 7.7.2021 г..
 */
@RestController
//@RequestMapping(path = "/api")
public class DnDCampainController {

    @Autowired
    private DnDCampaignRepository campaignRepository;

    @GetMapping(path = "/campaigns")
    public List<DnDCampaignView> findAll() {
        return campaignRepository.findAllPartial();
    }

    @PostMapping(path = "/campaigns")
    public DnDCampaign create(@RequestBody DnDCampaign campaign) {
        return this.campaignRepository.save(campaign);
    }

    @DeleteMapping(path = "/campaigns/{id}")
    public void deleteCampaign(@PathVariable String id) {
        Optional<DnDCampaign> campaign = campaignRepository.findById(id);

        if (campaign.isPresent()) {
            campaign.get().setCharacters(new ArrayList<>());
            campaign.get().setItems(new ArrayList<>());
            campaign.get().setNpcs(new ArrayList<>());
            campaignRepository.save(campaign.get());
            campaignRepository.deleteById(id);
            return;
        }
        throw new EntityNotFoundException("Campaign not found");
    }

    @GetMapping(path = "/campaigns/{id}")
    public Optional<DnDCampaign> getCampaign(@PathVariable String id) {
        return this.campaignRepository.findById(id);
    }
}
