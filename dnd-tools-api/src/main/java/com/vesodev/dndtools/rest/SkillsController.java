package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.SkillRepository;
import com.vesodev.dndtools.model.Skill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 24.6.2021 г..
 */
@RestController
//@RequestMapping(path = "/api")
public class SkillsController {

    @Autowired
    private SkillRepository skillRepository;

    @PutMapping(path = "/skills")
    public Skill updateSkill(@RequestBody Skill skill) {
        return this.skillRepository.save(skill);
    }
}
